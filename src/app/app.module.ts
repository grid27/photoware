import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { App } from './app.component';
import { GalleryPageModule } from "../pages/gallery/gallery.module"
import { Messages } from "../services/messages.service"
import { NativeStorage } from "@ionic-native/native-storage"
import { Toast } from "@ionic-native/toast"
import { HTTP } from "@ionic-native/http"
import { BarcodeScanner } from "@ionic-native/barcode-scanner"
import { Camera } from "@ionic-native/camera"
import { PhotoViewer } from "@ionic-native/photo-viewer"
import { File } from "@ionic-native/file"
import { Network } from "@ionic-native/network"
import { GalleryService } from "../services/gallary.service"
import {FileTransfer} from "@ionic-native/file-transfer"


@NgModule({
  declarations: [App],
  imports: [
    BrowserModule,
    GalleryPageModule,
    IonicModule.forRoot(App)
  ],
  bootstrap: [IonicApp],
  entryComponents: [App],
  providers: [
    StatusBar,
    SplashScreen,
    Messages,
    NativeStorage,
    Toast,
    HTTP,
    BarcodeScanner,
    Camera,
    PhotoViewer,
    File,
    Network,
    GalleryService,
    FileTransfer,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
