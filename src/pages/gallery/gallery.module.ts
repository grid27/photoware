import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GalleryPage } from './gallery';
import { ImagePage } from "./image/image-page"


@NgModule({
  declarations: [
    GalleryPage,
    ImagePage,
  ],
  imports: [
    IonicPageModule.forChild(GalleryPage),
  ],
  entryComponents: [
    ImagePage
  ]
})
export class GalleryPageModule {}
