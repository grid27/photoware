import { Component, OnInit } from '@angular/core';

import {
  ActionSheetController,
  IonicPage,
  ToastController,
  ModalController,
} from 'ionic-angular'

//import { PhotoViewer } from "@ionic-native/photo-viewer"
import { Camera } from "@ionic-native/camera"
import { File, Entry, FileError } from "@ionic-native/file"
import { Toast } from "@ionic-native/toast"
import { BarcodeScanner, BarcodeScanResult } from "@ionic-native/barcode-scanner"


import { Messages } from "../../services/messages.service"
import { GalleryService } from "../../services/gallary.service"

import { errorTypes } from "../../models/types"
import { Helpers } from "../../common/helpers.class"
import { Gallery, Image } from "../../models/gallery.model"
import { ImagePage } from "./image/image-page"

/**
 * Generated class for the GalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage implements OnInit {

  constructor(
    private camera: Camera,
    private file: File,
    private toastCtrl: ToastController,
    private actionSheetCtrl: ActionSheetController,
    private barcodescanner: BarcodeScanner,
    private modalCtrl: ModalController,
    private messages: Messages,
    private toast: Toast,
    public galleryService: GalleryService
  ) {

  }

  get gallery(): Gallery {
    return this.galleryService.gallery
  }

  ngOnInit() {

  }

  showImage(image) {
    let imageModal = this.modalCtrl.create(ImagePage, { image, gallery: this.gallery })
    imageModal.present()
  }

  ionViewDidLoad() {}

  actionSheetImage(image:Image) {
    let uploadButton = {
      text: 'Выгрузить',
      role: 'destructive',
      icon: 'md-cloud-done',
      handler: () => {
        this.galleryService.uploadImage(image)
      }
    }
    let buttons:any = [
      {
        text: 'Удалить',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.gallery.remove(image)
          Helpers.removeFile(image.path)
        }
      },
      {
        text: 'Отмена',
        role: 'cancel',
        icon: 'close'
      }
    ]
    if (!image.uploaded) {
      buttons = [uploadButton].concat(buttons)
    }
		this.actionSheetCtrl.create({
			title: image.barcode,
			buttons
		}).present()
	}

  onTakePhoto(barcode) {
    this.camera.getPicture({
			quality: 20,
      sourceType : 1,
      allowEdit : false,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true,
		})
		.then((imageData:any) => {
			const currentName = imageData.replace(/^.*[\\\/]/, '')
			const currentPath = imageData.replace(/[^\/]*$/, '')
      let ext = currentName.split(".")
      ext = ext[ext.length - 1]
			const newFileName = `${barcode}_${Date.now()}.${ext}`
			this.file.moveFile(currentPath, currentName, this.file.cacheDirectory, newFileName)
			.then((data: Entry) => {
				this.gallery.add({path: data.nativeURL, barcode})
        console.log(this.galleryService.gallery)  // FIXME
			})
			.catch((err: FileError) => {
				console.error("File.moveFile", err)
				this.toastCtrl.create({
					message: 'Неудалось сохранить изображение. Попробуйте еще раз',
					duration: 3000
				}).present()
			})
		})
		.catch(err => {
			console.error("Camera.getPicture", err)
			this.toastCtrl.create({
				message: 'Не получилось создать фотку. Попробуйте еще раз',
				duration: 5000
			}).present()
		})
  }

  onScanBarcode () {
		this.barcodescanner.scan().then((barcodeData:BarcodeScanResult) => {
			if (barcodeData.text) {
				let barcode = barcodeData.text
        this.onTakePhoto(barcode)
			} else {
				this.messages.toastErrors(errorTypes.BARCODE_ERROR)
			}
		}, (err) => {
			this.toast.show(err, '3000', 'center')
		});
	}

}
