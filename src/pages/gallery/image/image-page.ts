import { Component, OnInit } from '@angular/core';

import {
  ActionSheetController,
  NavParams,
  ViewController
} from 'ionic-angular'


import { Helpers } from "../../../common/helpers.class"
import { Gallery, Image } from "../../../models/gallery.model"
import { GalleryService } from "../../../services/gallary.service"

/**
 * Generated class for the GalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'image-page',
  templateUrl: 'image-page.html',
})
export class ImagePage implements OnInit {

  image: Image
  gallery: Gallery

  constructor(
    private viewCtrl: ViewController,
    public navParams: NavParams,
    private actionSheetCtrl: ActionSheetController,
    private galleryService: GalleryService
  ) {}

  ngOnInit() {
    this.image = this.navParams.get('image')
    this.gallery = this.navParams.get('gallery')
  }

  ionViewDidLoad() {}

  closeModal() {
    this.viewCtrl.dismiss()
  }

  actionSheetImage(image:Image) {
    let uploadButton = {
      text: 'Выгрузить',
      role: 'destructive',
      icon: 'md-cloud-done',
      handler: () => {
        this.galleryService.uploadImage(image)
      }
    }
    let buttons:any = [
      {
        text: 'Удалить',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.gallery.remove(image)
          Helpers.removeFile(image.path)
        }
      },
      {
        text: 'Отмена',
        role: 'cancel',
        icon: 'close'
      }
    ]
    if (!image.uploaded) {
      buttons = [uploadButton].concat(buttons)
    }
		this.actionSheetCtrl.create({
			title: image.barcode,
			buttons
		}).present()
	}

}
