import { ImageResizer } from '@ionic-native/image-resizer'
import { File } from '@ionic-native/file'


export class Helpers {
  /**
   * Хелпер класс
   * */
  static imageResizer = new ImageResizer()
  static file = new File()

  static resizeImg(path, callback) {
    // Создает миниатюру изображения
    let fileName = path.split("/").slice(-1)[0].split(".")
    fileName = fileName.slice(0, -1).join(".") + "_thumb." + fileName[fileName.length - 1]
    this.imageResizer.resize({
      uri: path,
      folderName: Helpers.file.cacheDirectory,
      quality: 90,
      width: 400,
      height: 400,
      fileName
    }).then(thumb => {
      callback(thumb)
    }).catch(e => {
      console.error("Ошибка получения миниатюры", e)
    })
  }

  static splitPathFile(url) {
    // Отделяет имя файла от пути, возвращяет массив [Путь, Имя файла]
    let pathArr = url.split("/")
    let filename = pathArr[pathArr.length - 1]
    let path = pathArr.slice(0, -1).join("/") + "/"
    return [path, filename]
  }

  static removeFile(filePath) {
    // Удаляет файл
    return new Promise((resolve, reject)=>{
      let [path, filename] = Helpers.splitPathFile(filePath)
      Helpers.file.removeFile(path, filename)
        .then(()=>{
          resolve()
        })
        .catch(
          (e) => {
            console.warn(e.message)
            reject(e)
          }
        )
    })
  }

  static checkFile(filePath, callbackY: Function = null, callbackN: Function = null) {
    // Проверака на существование файла
    let [path, filename] = Helpers.splitPathFile(filePath)
    this.file.checkFile(path, filename).then(() => {
      if (callbackY) callbackY()
    }).catch(() => {
      if (callbackN) callbackN()
    })
  }

  static compareObjects(newObj, oldObj) {
    // Сравнивает два объекта и возвращает расхождения
    let clone = "function" === typeof newObj.pop ? [] : {},
      changes = 0,
      prop = null,
      result = null,
      check = function (o1, o2) {
        for (prop in o1) {
          if (!o1.hasOwnProperty(prop)) continue;
          if (o1[prop] instanceof Date) {
            if (!(o2[prop] instanceof Date && o1[prop].getTime() == o2[prop].getTime())) {
              clone[prop] = newObj[prop]
              changes++
            }
          } else if (o1[prop] && o2[prop] && "object" === typeof o1[prop]) {
            if (result = Helpers.compareObjects(newObj[prop], oldObj[prop])) {
              clone[prop] = "function" === typeof o1[prop].pop ? newObj[prop] : result
              changes++
            }
          } else if (o1[prop] !== o2[prop]) {
            clone[prop] = newObj[prop]
            changes++
          }
        }
      };
    check(newObj, oldObj)
    check(oldObj, newObj)
    return changes ? clone : false;
  }
}
