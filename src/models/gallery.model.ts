import { Helpers } from "../common/helpers.class"
import { Model } from './model.class'
import _ from 'lodash';


export interface Image {
   /**
   * Модель Фотографий
   * */
  readonly barcode: string,
  path: string,
  thumb: string
  uploaded: boolean
}

export class Gallery extends Model implements Iterable<Image> {

  /**
   * Итератор фоток
   * */

  constructor(
    public images: Image[] = []
  ) {
    super();
  }

  filter(clb){
    return this.images.filter(clb)
  }

  get uploadLength(){
    return this.images.filter(x=>{return x.uploaded}).length
  }

  get noUploadLength(){
    return this.images.filter(x=>{return !x.uploaded}).length
  }

  cleanCache() {
    this.images = this.filter((x:Image)=> {
      if(x.uploaded) {
        Helpers.removeFile(x.path)
        return false
      }
      return true
    })
    this.forceUpdate()
  }

  add({path, barcode}) {
    Helpers.checkFile(path, function () {
      let image = {
        barcode: barcode,
        path,
        thumb: null,
        uploaded: false
      }
      Helpers.resizeImg(path,
        (thumb) => {
          image.thumb = thumb
          let oldImage = _.find(this.images, x => {return x.barcode == image.barcode})
          if(oldImage){
            const index = this.images.indexOf(oldImage)
            this.images[index] = image
          }
          else{
            this.images.push(image)
          }
          this.forceUpdate(image)
        }
      )
    }.bind(this))
  }

  remove(image:string|Image) {
    const path = typeof image == 'string'? image: image.path;
    this.images = _.filter(this.images, x => {return x.path != path})
    this.forceUpdate()
  }

  [Symbol.iterator]():Iterator<Image>{
    let pointer = 0
    let images = this.images

    return {

      next(): IteratorResult<Image> {
        if (pointer < images.length) {
          return {
            done: false,
            value: images[pointer++]
          }
        } else {
          return {
            done: true,
            value: null
          }
        }
      }

    }
  }
}
