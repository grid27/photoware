// Типы Ошибок
export const errorTypes = {
  NO_CONNECT: "NO_CONNECT", // Нет соединения
  BARCODE_ERROR: "BARCODE_ERROR", // Ошибка ввода штрихкода
  LOAD_ERROR: "LOAD_ERROR", // Ошибка загрузки
  UPLOAD_ERROR: "UPLOAD_ERROR",  // Ошибка выгрузки изображения
  TIMEOUT_CONNECT_ERROR: "TIMEOUT_CONNECT_ERROR", // Превышенно время ожидание загрузки
  UNDEFINED_ERROR: "UNDEFINED_ERROR", // Низвестная ошибка
}
