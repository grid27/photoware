import { AppConfig } from '../config'

export class Urls {

  static get uploadImageApi() {
    return AppConfig.serverUrl + 'api/barcode-ware/unload-image'
  }

}
