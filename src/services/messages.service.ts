import { Injectable } from '@angular/core'
import { AlertController } from 'ionic-angular'
import { Toast } from "@ionic-native/toast"
import { errorTypes } from '../models/types'


@Injectable()
export class Messages {

  constructor(
    public alertCtrl: AlertController,
    private toast: Toast
  ){}

  toastErrors(type){
    switch (type){
      case errorTypes.NO_CONNECT:
        this.toast.show("Нет соединения! Подключитесь к сети wifi", '6000', 'center').subscribe()
        break
      case errorTypes.BARCODE_ERROR:
        this.toast.show("Введите или отсканируйте свой Штрихкод", '6000', 'center').subscribe()
        break
      case errorTypes.TIMEOUT_CONNECT_ERROR:
        this.toast.show('Превышено время ожидание ответа', '5000', "center").subscribe()
        break
      case errorTypes.LOAD_ERROR:
        this.toast.show("Ошибка загрузки данных", '3000', 'center').subscribe()
        break
      default:
        this.toast.show("Неизвестная ошибка", '3000', "center").subscribe()
    }
  }

  messageEnableNetwork() {
	  this.alertCtrl.create({
      title: 'Подключитесь к сети!',
      buttons: [
        {
          text: "ОК",
          role: 'cancel',
        }
      ]
    }).present()
  }

  openMessage(title, message:string="",  callback:any=null){
    this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: "Закрыть",
          role: 'cancel',
          handler:() => {
            if (callback) callback()
          }
        }
      ]
    }).present()
  }

  openMessageCallback(title, message, callbackYes, callbackNo=()=>{}){
    this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: "Да",
          role: 'cancel',
          handler:() => callbackYes()
        },
        {
          text: "Нет",
          role: 'cancel',
          handler:() => callbackNo()
        }
      ]
    }).present()
  }

}
