import { Injectable } from '@angular/core'
import { NativeStorage } from "@ionic-native/native-storage"
import { HTTP } from "@ionic-native/http"
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Network } from '@ionic-native/network'
import { AppConfig } from "../config"
import { errorTypes } from "../models/types"
import { Messages } from "./messages.service"
import { Urls } from "../models/urls.model"
import { Platform } from "ionic-angular"
import { Gallery, Image } from "../models/gallery.model"


@Injectable()
export class GalleryService {

  gallery: Gallery
  flagAutoUpload: boolean = true
  flagAutoCleanCache: boolean = true

  flagLoad = true

  IDInterval:number
  INTERVAL_AUTOCLEAN = 1000*60*5

  constructor(
    private messages: Messages,
    private http: HTTP,
    private storage: NativeStorage,
    private network: Network,
    private platform: Platform,
    private transfer: FileTransfer
  ){
    this.platform.ready().then( () => {
      this.http.setHeader("Authorization", "Token " + AppConfig.ApiKey)
      this.loadImages()
    })
  }

  autoCleanCache(timePassed=0) {
    let interval = 1000*15
    this.IDInterval = setInterval(() => {
      if (this.INTERVAL_AUTOCLEAN <= timePassed) {
        this.gallery.cleanCache()
        timePassed = interval
      }
      else {
        timePassed += interval
        this.storage.setItem("timePassed", timePassed)
      }
    }, interval)
  }

  toggleAutoUpload() {
    this.storage.setItem("flagAutoUpload", this.flagAutoUpload)
  }

  toggleAutoCleanCache() {
    if(!this.flagAutoCleanCache){
      clearInterval(this.IDInterval)
    }
    else {
      this.autoCleanCache()
    }
    this.storage.setItem("flagAutoCleanCache", this.flagAutoCleanCache)
  }

  async loadImages() {
    try {
      let images = await this.storage.getItem("images")
      this.gallery = new Gallery(images)
      try{
        this.flagAutoUpload = await this.storage.getItem("flagAutoUpload")
      }
      catch (e){
        this.flagAutoUpload  = true
      }
      try{
        this.flagAutoCleanCache = await this.storage.getItem("flagAutoCleanCache")
      }
      catch (e){
        this.flagAutoCleanCache = true
      }
      if (this.flagAutoCleanCache) {
        let timePassed = await this.storage.getItem("timePassed")
        this.autoCleanCache(timePassed)
      }
      return true
    }
    catch(e) {
      this.gallery = new Gallery()
    }
    finally {
      this.gallery.subscribe(this.saveImage.bind(this))
    }
  }

  saveImage(gallery, image) {
    // Сохранение изменений
    if (this.flagAutoUpload && image) {
      this.uploadImage(image)
    }
    this.storage.setItem("images", gallery.images)
  }

  async uploadImage(image:Image) {
    if(this.network.type == "wifi") {
      this.flagLoad = true;
      try {
        const fileTransfer: FileTransferObject = this.transfer.create()
        let ext = image.path.match(/.[\w]+$/)[0]
        let result = await fileTransfer.upload(
          image.path, Urls.uploadImageApi,
          {
            fileKey: "photo",
            fileName: image.barcode + ext,
            headers: {
              "Authorization": "Token " + AppConfig.ApiKey
            }
          },
        )

        const response = JSON.parse(result.response)
        if (response.success) {
          image.uploaded = true
          this.gallery.forceUpdate()
          return true
        }
        else {
          this.messages.toastErrors(errorTypes.UPLOAD_ERROR)
          throw errorTypes.UPLOAD_ERROR
        }
      }
      catch (e) {
        console.error(e)
        this.messages.toastErrors(errorTypes.UPLOAD_ERROR)
        throw errorTypes.UPLOAD_ERROR
      }
      finally {
        this.flagLoad = false
      }
    }
    else {
      this.messages.toastErrors(errorTypes.NO_CONNECT)
      throw errorTypes.NO_CONNECT
    }
  }
}
